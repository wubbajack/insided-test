<?php namespace Tests\Objects;

use TestCase;

use App\Core\Data\Models\Post;
use App\Core\Data\Repositories\PostsRepository;

use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class PostsTest
 *
 * @author    Chris Stolk <chris@superherocheesecake.com>
 * @copyright Superhero Cheesecake
 * @since     02/07/16 15:18
 */
class PostsTest extends TestCase
{

    /**
     * Tests the posts repository
     */
    public function testPostsRepository()
    {
        $repo = new PostsRepository(new Post(), $this->app);

        // Check if creating a new record results in a new Post
        // and whether the count is upped (actually added to the DB)
        $count = $repo->count();
        $model = $repo->create([
            'title' => 'test',
            'image_type' => 1,
            'image_thumbnail' => '1/2/test.jpg',
            'image_full' => '1/2/full.jpg'
        ]);
        $this->assertInstanceOf(Post::class, $model, 'Model not an instance of a post');
        $this->assertGreaterThan($count, $repo->count(), 'Model wasn\'t added successfully. Count has not been upped');

        // Test whether the model is actually present in our datasource
        $model = $repo->find($model->getKey());
        $this->assertTrue($model === null || $model instanceof Post, 'Model is not null or an instance of a Post');

        // Update the model
        $model = $repo->update(['image_type' => 2], $model->getKey());
        $this->assertTrue($model->image_type == 2, 'Model wasn\'t updated correctly');

        // List all available models with key title and id
        $list = $repo->lists('title', 'id');
        $this->assertNotEmpty($list, 'List is empty');

        // Paginate the result and test whether the paginator contains all the correct details
        $paginator = $repo->paginate(25, 1);
        $this->assertInstanceOf(LengthAwarePaginator::class, $paginator);
        $this->assertEquals(1, $paginator->currentPage(), 'Paginator is not at page 1');
        $this->assertGreaterThan(0, count($paginator->items()));

        // Test count decrement and delete
        $count = $repo->count();
        $repo->delete($model->getKey());
        $this->assertNull($repo->find($model->getKey()), 'Model is still present in the datasource');
        $this->assertLessThan($count, $repo->count(), 'Count was not decremented after delete');
    }
}
