<?php namespace App\Frontend\Controllers;

use Hashids\Hashids;

use App\Core\Data\Models\Post;
use App\Core\Data\Repositories\PostsRepository;

use Illuminate\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PostsController
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Frontend\Controllers
 * @copyright Chris Stolk
 * @since     03/07/16 15:04
 */
class PostsController extends Controller
{

    /**
     * @var PostsRepository
     */
    protected $repo;

    /**
     * @var Hashids
     */
    protected $hasher;

    /**
     * FrontController constructor.
     *
     * @param PostsRepository $repo
     * @param Hashids         $hasher
     */
    public function __construct(
        PostsRepository $repo,
        Hashids $hasher
    ) {
        $this->repo = $repo;
        $this->hasher = $hasher;
    }

    /**
     * Returns the index page
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return response(
            view('layouts.main', [
                'header'   => view('partials.main.header', [
                    'posts' => $this->repo->count(),
                    'views' => $this->repo->getTotalViews()
                ]),
                'page'     => $this->createPostsPage()
            ])
        );
    }

    /**
     * Returns a single post
     * @param  string $key
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function single($key)
    {
        $key = Post::decodeKey($key);
        if (!$post = $this->repo->find($key)) {
            throw new NotFoundHttpException('Post not found');
        }

        $post = $this->repo->incrementViewsForPost($post);
        return response(
            view('layouts.main', [
                'header'   => view('partials.main.header', [
                    'posts' => $this->repo->count(),
                    'views' => $this->repo->getTotalViews()
                ]),
                'page'     => $this->createSinglePostPage($post)
            ])
        );
    }

    /**
     * Creates the posts page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function createPostsPage()
    {
        return view('pages.posts', [
            'form'     => view('partials.posts.form'),
            'posts'    => $this->repo->allOrderedByNewest(),
            'template' => view('partials.posts.template_js')
        ]);
    }

    /**
     * Creates the single post page
     *
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function createSinglePostPage(Post $post)
    {
        return view('pages.post', [
            'post' => view('partials.posts.single_template', ['post' => $post])
        ]);
    }

    /**
     * Decodes the key
     *
     * @param  string $key
     * @return string
     */
    protected function decodeKey($key)
    {
        $decoded_key = $this->hasher->decode($key);
        if (!is_array($decoded_key)) {
            throw new \InvalidArgumentException('Key is invalid');
        }

        return array_shift($decoded_key);
    }
}
