<?php namespace App\Frontend\Controllers;

use View;
use Request;
use Illuminate\Routing\Controller;

/**
 * Class BaseController
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Frontend\Controllers
 * @copyright Chris Stolk
 * @since     01/03/16 22:50
 */
class BaseController extends Controller
{

    /**
     * @var View
     */
    protected $layout;

    /**
     * @var bool
     */
    protected $is_mobile;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {

        //View::setIsMobile($this->is_mobile);

        View::share('url_main', config('metadata.urls.main'));
        View::share('url_images', config('metadata.urls.images'));
        View::share('url_css', config('metadata.urls.css'));
        View::share('url_js', config('metadata.urls.js'));
        View::share('url_uploads', config('metadata.urls.uploads'));
        View::share('url_generated', config('metadata.urls.generated'));
        //View::share('device_type', Device::getType());

        //View::share('environment', App::environment());
        View::share('this_url', Request::url());
    }

    /**
     * Returns the layout data
     *
     * @return array
     */
    protected function getLayoutData()
    {
        return [
            'title'            => config('metadata.title'),
            'site_title'       => config('metadata.title'),
            'description'      => config('metadata.description'),
            'keywords'         => config('metadata.keywords'),
            'og_image'         => config('metadata.og_image'),
            'twitter_site'     => config('metadata.twitter.site'),
            'twitter_creator'  => config('metadata.twitter.creator'),
            'cache_buster'     => config('metadata.cache_buster'),
            'fb_admin_ids'     => config('metadata.fb.admin_ids'),
            'fb_app_id'        => config('metadata.fb.app_id'),
            'ga_ua_id'         => config('services.ga_ua_id'),
//            'device'           => Device::toJson(),
            'minified_sources' => App::environment('staging', 'production'),
        ];
    }

    /**
     * Makes the layout
     */
    protected function makeLayout()
    {
        $this->layout = View::make('layouts.main');
    }
}
