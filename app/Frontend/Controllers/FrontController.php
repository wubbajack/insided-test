<?php namespace App\Frontend\Controllers;

use Symfony\Component\HttpFoundation\Response as HttpResponse;

/**
 * Class FrontController
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Frontend\Controllers
 * @copyright Chris Stolk
 * @since     01/03/16 22:50
 */
class FrontController extends BaseController
{

    /**
     * Returns the index page
     *
     * @return Response
     */
    public function getIndex()
    {
        return response(
            view('layouts.main', [
                
            ])
        );
    }

    /**
     * Returns the 404 page
     *
     * @return Response
     */
    public function get404Index()
    {
        return response(view('errors.404'), HttpResponse::HTTP_NOT_FOUND);
    }
}
