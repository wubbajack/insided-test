<?php

/*
|--------------------------------------------------------------------------
| Website routes
|--------------------------------------------------------------------------
|
| All routes related to the frontend of the site should be
| registered in this file.
|
| API routes can be registered under app/Api/routes.php
|
*/

Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'PostsController@index');
    Route::get('/posts/{key}', [
        'as'   => 'posts.single',
        'uses' => 'PostsController@single',
    ]);

});
