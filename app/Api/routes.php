<?php

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
|
| All routes related to the API should be registered here.
| The defaults are:
| - prefix          api
| - middleware      api (group of middlewares)
|                   Contains throttler and CSRF protection header.
|                   Sessions and cookie decryption are also available
|                   meaning this API is not stateless.
|
*/

Route::group([
    'prefix'     => 'api',
    'middleware' => ['api']
], function () {


    // Retunrs the meta information related to the posts resource
    Route::get('posts/meta', 'PostsController@meta');

    Route::resource(
        'posts',
        'PostsController',
        ['only' => ['index', 'show', 'store']]
    );

    Route::get('posts/export/{type}', [
        'as'   => 'posts.export',
        'uses' => 'ExportsController@export'
    ]);
});
