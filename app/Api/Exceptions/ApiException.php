<?php namespace App\Api\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ApiException
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Api\Exceptions
 * @copyright Chris Stolk
 * @since     16/03/16 20:23
 */
class ApiException extends HttpException
{

    /**
     * Make sure an api exception always has content type JSON
     * @return array
     */
    public function getHeaders()
    {
        return array_merge(parent::getHeaders(), ['Content-Type' => 'application/json']);
    }
}
