<?php namespace App\Api\Http\Requests;

class StorePostRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['string', 'max:100'],
            'image' => ['required', 'file', 'mimes:jpeg,png,gif', 'image_size:1920,1080', 'filesize:20,mb']
        ];
    }
}
