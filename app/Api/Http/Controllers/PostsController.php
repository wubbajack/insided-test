<?php namespace App\Api\Http\Controllers;

use App\Core\Data\Models\Post;

use App\Api\Exceptions\ApiException;
use App\Api\Http\Requests\StorePostRequest;

use App\Api\Http\Presenters\PostsPresenter;
use App\Api\Http\Pagination\CollectionPaginator;
use App\Api\Http\Response\Factory as ApiResponse;

use App\Core\Data\Repositories\PostsRepository;

use Illuminate\Http\Request;

/**
 * Class PostsController
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Api\Http\Controllers
 * @copyright Chris Stolk
 * @since     02/07/16 14:47
 */
class PostsController extends ApiController
{

    /**
     * @var PostsRepository
     */
    protected $repo;

    /**
     * PostsController constructor.
     *
     * @param Request         $request
     * @param ApiResponse     $response
     * @param PostsRepository $repo
     */
    public function __construct(
        Request $request,
        ApiResponse $response,
        PostsRepository $repo
    ) {
        parent::__construct($request, $response);
        $this->repo   = $repo;
    }

    /**
     * Returns the posts collection, paginated
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collectionPaginator = CollectionPaginator::createFromRequest($this->request);

        return $this->response->paginated(
            $this->repo->paginate($collectionPaginator->getPerPage() ?: 25, $collectionPaginator->getCurrentPage() + 1),
            PostsPresenter::class
        );
    }

    /**
     * Returns meta information related to the collection of "post" resources.
     * Specfically:
     *  - The total posts
     *  - The total views
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function meta()
    {
        return response()->json(
            [
                'total' => (int) $this->repo->count(),
                'views' => (int) $this->repo->getTotalViews()
            ]
        );
    }

    /**
     * Returns a single item
     *
     * @param  string $key
     * @return \Illuminate\Http\Response
     */
    public function show($key)
    {
        if (!$post = $this->repo->find(Post::decodeKey($key))) {
            throw new ApiException(404, 'Resource not found');
        }

        return $this->response->item($post, PostsPresenter::class);
    }

    /**
     * Stores the post
     *
     * @param StorePostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        try {
            $post = $this->repo->createFromRequest($request);
        } catch (\Exception $e) {
            throw new ApiException(500, 'Unable to store post', $e);
        }

        return $this->response->item($post, PostsPresenter::class);
    }
}
