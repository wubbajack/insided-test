<?php namespace App\Api\Http\Controllers;

use App\Api\Http\Pagination\CollectionPaginator;
use App\Api\Http\Response\Factory as ApiResponse;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

/**
 * Class ApiController
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Api\Http\Controllers
 * @copyright Chris Stolk
 * @since     16/03/16 21:22
 */
abstract class ApiController extends Controller
{

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ApiResponse
     */
    protected $response;

    /**
     * ApiController constructor.
     *
     * @param Request     $request
     * @param ApiResponse $response
     */
    public function __construct(Request $request, ApiResponse $response)
    {
        $this->request  = $request;
        $this->response = $response;
    }

    /**
     * Checks whether the request is paginated. Should only be used when returning collections, not single items
     *
     * @return bool
     */
    protected function isRequestPaginated()
    {
        $paginator_headers = [
            CollectionPaginator::HEADER_PER_PAGE,
            CollectionPaginator::HEADER_TOTAL,
            CollectionPaginator::HEADER_OFFSET,
        ];

        foreach ($paginator_headers as $header) {
            if (!$this->request->hasHeader($header)) {
                return false;
            }
        }

        return true;
    }
}
