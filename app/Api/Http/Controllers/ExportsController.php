<?php namespace App\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Container\Container;

use ZipStream\ZipStream;

use App\Core\Exporter\CsvExporter;
use App\Core\Exporter\Exports\PostsExport;
use App\Core\Data\Repositories\PostsRepository;

use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class ExportsController
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Api\Http\Controllers
 * @copyright Chris Stolk
 * @since     03/07/16 16:46
 */
class ExportsController extends Controller
{

    /**
     * @var string
     */
    const TYPE_ZIP = 'zip';

    /**
     * @var string
     */
    const TYPE_CSV = 'csv';

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var PostsRepository
     */
    protected $repo;

    /**
     * ExportsController constructor.
     *
     * @param Request         $request
     * @param Container       $container
     * @param PostsRepository $repo
     */
    public function __construct(
        Request $request,
        Container $container,
        PostsRepository $repo
    ) {
        $this->request   = $request;
        $this->container = $container;
        $this->repo      = $repo;
    }

    /**
     * Exports the posts
     *
     * @param $type
     * @return StreamedResponse
     */
    public function export($type)
    {
        if (!$this->validateType($type)) {
            throw new BadRequestHttpException('Unsupported export type');
        }

        switch ($type) {
            case self::TYPE_CSV:
                return $this->returnCsvExport();
            case self::TYPE_ZIP:
                return $this->returnZipStream();
            default:
                return response(null, 204);
        }
    }

    /**
     * Returns the CSV export
     *
     * @return StreamedResponse
     */
    protected function returnCsvExport()
    {
        /**
         * @var $exporter CsvExporter
         */
        $exporter = $this->container->make(CsvExporter::class);

        return response()->stream(function () use ($exporter) {
            $exporter->output(new PostsExport($this->repo->getModel()));
        }, 200, [
            'Content-Type' => 'text/csv',
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => 'attachment; filename=export_posts'.time().'.csv',
            'Content-Transfer-Encoding' => 'binary',
            'Pragma' => 'no-cache'
        ]);
    }

    /**
     * Returns a zipfile for download
     *
     * @return StreamedResponse
     */
    protected function returnZipStream()
    {
        /**
         * @var $exporter CsvExporter
         */
        $exporter = $this->container->make(CsvExporter::class);
        $filename = 'export_posts_'. time() . '.zip';
        $zip      = new ZipStream('export_posts_'. time() . '.zip', [
            ZipStream::OPTION_SEND_HTTP_HEADERS => false
        ]);

        return response()->stream(function () use ($exporter, $zip) {
            $export_stream = tmpfile();
            $exporter->save(new PostsExport($this->repo->getModel()), $export_stream);
            $zip->addFileFromStream(
                'export_posts.csv',
                $export_stream
            );

            $this->repo->chunk(25, function ($posts) use ($zip) {
                foreach ($posts as $post) {
                    $zip->addFileFromPath(
                        'images/'. $post->getAttribute('image_full'),
                        public_path('content/'. $post->getAttribute('image_full'))
                    );
                }
            });

            $zip->finish();
        }, 200, [
            'Content-Type' => 'application/x-zip',
            'Content-Disposition' => 'attachment; filename='.$filename,
            'Content-Transfer-Encoding' => 'binary',
            'Pragma' => 'no-cache'
        ]);
    }

    /**
     * Validates the type
     *
     * @param  string $type
     * @return bool
     */
    protected function validateType($type)
    {
        return in_array($type, [self::TYPE_CSV, self::TYPE_ZIP]);
    }
}
