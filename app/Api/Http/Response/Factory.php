<?php namespace App\Api\Http\Response;

use App\Api\Http\Presenters\PresenterContract;
use App\Api\Http\Pagination\CollectionPaginator;

use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class Factory
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Api\Http\Response
 * @copyright Chris Stolk
 * @since     16/03/16 21:30
 */
class Factory
{

    /**
     * @var Container
     */
    protected $container;

    /**
     * Factory constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Returns a collection as a response
     *
     * @param  Collection|array         $collection
     * @param  PresenterContract|string $presenter
     * @return Response
     */
    public function collection($collection, $presenter)
    {
        if (!($collection instanceof Collection)) {
            $collection = Collection::make($collection);
        }

        return $this->createResponse($this->makePresenter($presenter)->collection($collection), 200);
    }

    /**
     * Returns a single item as a response
     *
     * @param  mixed                    $item
     * @param  PresenterContract|string $presenter
     * @return Response
     */
    public function item($item, $presenter)
    {
        return $this->createResponse($this->makePresenter($presenter)->single($item), 200);
    }

    /**
     * Returns a presented paginated collection
     *
     * @param LengthAwarePaginator     $paginator
     * @param PresenterContract|string $presenter
     * @return Response
     */
    public function paginated(LengthAwarePaginator $paginator, $presenter)
    {
        return $this->createResponse(
            $this->makePresenter($presenter)->collection($paginator->getCollection()),
            200
        )->withHeaders(CollectionPaginator::makeResponseHeaders($paginator));
    }

    /**
     * Returns a bad request response
     *
     * @param  string $message
     * @return Response
     */
    public function badRequest($message = null)
    {
        return $this->error($message, Response::HTTP_BAD_REQUEST);
    }

    /**
     * Returns an unauthorized response
     *
     * @param  string $message
     * @return Response
     */
    public function unauthorized($message = null)
    {
        return $this->error($message, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Returns a forbidden response
     *
     * @param  string $message
     * @return Response
     */
    public function forbidden($message = null)
    {
        return $this->error($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * Returns a not found response
     *
     * @param  string $message
     * @return Response
     */
    public function notFound($message = null)
    {
        return $this->error($message, Response::HTTP_NOT_FOUND);
    }

    /**
     * Returns an error response
     *
     * @param string $message     The actual error itself
     * @param int    $status_code The status code of the error
     * @param array  $errors      Additional errors to return
     * @return Response
     */
    public function error($message, $status_code, array $errors = [])
    {
        return $this->createResponse(
            [
                'message'     => $message?: Response::$statusTexts[$status_code],
                'status_code' => $status_code,
                'errors'      => $errors
            ],
            $status_code ?: Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

    /**
     * Creates the response object
     *
     * @param  mixed $data
     * @param  int $status_code
     * @return Response
     */
    protected function createResponse($data, $status_code)
    {
        return response($data, $status_code, ['Content-Type' => 'application/json']);
    }

    /**
     * Makes the presenter
     *
     * @param  PresenterContract|string $presenter
     * @return PresenterContract
     */
    protected function makePresenter($presenter)
    {
        if (!($presenter instanceof PresenterContract)) {
            $presenter = $this->container->make($presenter);
        }

        return $presenter;
    }
}
