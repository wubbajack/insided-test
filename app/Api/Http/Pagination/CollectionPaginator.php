<?php namespace App\Api\Http\Pagination;

use Illuminate\Http\Request;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class CollectionPaginator
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Api\Pagination
 * @copyright Chris Stolk
 * @since     02/07/16 17:48
 */
class CollectionPaginator
{

    const HEADER_PER_PAGE = 'Collection-Per-Page';
    const HEADER_OFFSET   = 'Collection-Offset';
    const HEADER_TOTAL    = 'Collection-Total';

    /**
     * @var int
     */
    protected $per_page;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var int
     */
    protected $current_page;

    /**
     *
     * @param  Request $request
     * @return static
     */
    public static function createFromRequest(Request $request)
    {
        $instance = new static();

        $instance->per_page     = (int) $request->header(self::HEADER_PER_PAGE, 0);
        $instance->offset       = (int) $request->header(self::HEADER_OFFSET, 0);

        if ($instance->offset > 0) {
            $instance->current_page = (int) round($instance->offset / $instance->per_page, 0, PHP_ROUND_HALF_UP);
        }

        return $instance;
    }

    /**
     * Creates the response headers for a paginated collection
     *
     * @param LengthAwarePaginator $paginator
     * @return array
     */
    public static function makeResponseHeaders(LengthAwarePaginator $paginator)
    {
        return [
            self::HEADER_PER_PAGE => (int) $paginator->perPage(),
            self::HEADER_OFFSET   => (int) ($paginator->currentPage() - 1) * $paginator->perPage(),
            self::HEADER_TOTAL    => (int) $paginator->total()
        ];
    }

    /**
     * Returns the requested items per page
     *
     * @return int
     */
    public function getPerPage()
    {
        return $this->per_page;
    }

    /**
     * Returns the offset
     *
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Returns the current page
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->current_page;
    }
}
