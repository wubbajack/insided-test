<?php namespace App\Api\Http\Presenters;

use App\Core\Data\Models\Post;

/**
 * Class PostsPresenter
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Api\Http\Presenters
 * @copyright Chris Stolk
 * @since     02/07/16 17:59
 */
class PostsPresenter extends Presenter
{

    /**
     * Presents a single item for an Api response
     *
     * @param  Post $item
     * @return array
     */
    public function single($item)
    {
        return [
            'id'        => $item->getEncodedKey(),
            'url'       => route('posts.single', $item->getEncodedKey()),
            'views'     => (int)$item->getAttribute('views'),
            'title'     => $item->getAttribute('title'),
            'image'     => [
                'thumbnail' => url('content/' . $item->getAttribute('image_thumbnail')),
                'full'      => url('content/' . $item->getAttribute('image_full'))
            ],
            'posted_on' => $item->created_at->toRfc3339String()
        ];
    }
}
