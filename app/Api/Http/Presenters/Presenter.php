<?php namespace App\Api\Http\Presenters;

use Illuminate\Support\Collection;

/**
 * Class Presenter
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Api\Http\Presenters
 * @copyright Chris Stolk
 * @since     16/03/16 21:59
 */
abstract class Presenter implements PresenterContract
{

    /**
     * Presents a collection of items
     *
     * @param  Collection $collection
     * @return array
     */
    public function collection(Collection $collection)
    {
        return $collection->map(function ($item) {
            return $this->single($item);
        })->toArray();
    }
}
