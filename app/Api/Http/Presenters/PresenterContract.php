<?php namespace App\Api\Http\Presenters;

use Illuminate\Support\Collection;

/**
 * Interface PresenterContract
 *
 * @author    Chris Stolk
 * @package   App\Api\Http\Presenters
 * @copyright Chris Stolk
 * @since     16/03/16 21:14
 */
interface PresenterContract
{

    /**
     * Presents an entire collection for an Api response
     *
     * @param  Collection $collection
     * @return array
     */
    public function collection(Collection $collection);

    /**
     * Presents a single item for an Api response
     *
     * @param  $item
     * @return array
     */
    public function single($item);
}
