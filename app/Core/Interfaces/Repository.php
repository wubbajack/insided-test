<?php namespace App\Core\Interfaces;

/**
 * Class Repository
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   Core\Contracts
 * @copyright Chris Stolk
 * @since     02/07/16 14:51
 */
interface Repository
{

    /**
     * Returns all records
     *
     * @return mixed
     */
    public function all();

    /**
     * Finds a particular record for key
     *
     * @param  mixed $key
     * @return mixed
     */
    public function find($key);

    /**
     * Lists a certain value (and key)
     *
     * @param string $value
     * @param null   $key
     * @return mixed
     */
    public function lists($value, $key = null);

    /**
     * Paginates the result
     *
     * @param  int      $per_page
     * @param  int|null $page
     * @return mixed
     */
    public function paginate($per_page, $page = null);

    /**
     * Creates a new model
     *
     * @param  array $attributes
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * Updates the attributes of a set of models based on key and key attribute
     *
     * @param  array  $attributes
     * @param  mixed  $key
     * @param  string $key_attribute
     * @return mixed
     */
    public function update(array $attributes, $key, $key_attribute = null);

    /**
     * Deletes a record with a specific key
     *
     * @param  mixed $key
     * @return mixed
     */
    public function delete($key);
}
