<?php namespace App\Core\Interfaces\Exporter;

/**
 * Class ExporterInterface
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Core\Interfaces
 * @copyright Chris Stolk
 * @since     03/07/16 16:50
 */
interface ExporterInterface
{

    /**
     * Exports the data
     *
     * @param  mixed $target
     * @return mixed
     */
    public function export($target);
}
