<?php namespace App\Core\Interfaces\Exporter;

/**
 * Class CsvExporterInterface
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Core\Interfaces\Exporter
 * @copyright Chris Stolk
 * @since     03/07/16 16:52
 */
interface CsvExporterInterface extends ExporterInterface
{

    /**
     * Returns a single line
     *
     * @return array
     */
    public function getLine();
}
