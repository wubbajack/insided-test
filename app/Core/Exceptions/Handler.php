<?php namespace App\Core\Exceptions;

use Exception;
use App\Api\Exceptions\ApiException;

use Illuminate\Http\Response;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     *
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ApiException) {
            return $this->createApiExceptionResponse($e);
        }

        if ($e instanceof NotFoundHttpException) {
            return app('App\Frontend\Controllers\FrontController')->get404Index();
        }

        return parent::render($request, $e);
    }

    /**
     * Creates the api exception response
     *
     * @param ApiException $e
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function createApiExceptionResponse(ApiException $e)
    {
        $body = ['error' => $this->getApiExceptionMessage($e)];

        // Add debug details if the error occurs
        if (config('app.debug')) {
            $body['details'] = [
                'line'  => $e->getLine(),
                'file'  => $e->getFile(),
                'code'  => $e->getCode(),
                'trace' => $e->getTrace(),
            ];
        }

        return response($body, $e->getStatusCode(), $e->getHeaders());
    }

    /**
     * Returns the api exception message.
     * If debug mode is enabled, the message set in the code is returned. If not, the default HTTP status message
     * will be returned.
     *
     * @param  ApiException $e
     *
     * @return string
     */
    protected function getApiExceptionMessage(ApiException $e)
    {
        return config('app.debug') ? $e->getMessage() : Response::$statusTexts[$e->getStatusCode()];
    }
}
