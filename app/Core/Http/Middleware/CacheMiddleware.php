<?php

namespace App\Core\Http\Middleware;

use App;
use Cache;
use Closure;

/**
 * Class CacheMiddleware
 *
 * @author    Sara Gerion <sara@superherocheesecake.com>
 * @package   App\Http\Middleware
 * @copyright Sara Gerion
 */
class CacheMiddleware
{
    /**
     * @param         $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // Disable cache for local and development environment
        if (App::environment('local', 'development')) {
            Cache::flush();
        }

        return $response;
    }
}
