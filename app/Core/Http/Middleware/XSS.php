<?php namespace App\Core\Http\Middleware;

use Closure;

/**
 * Class XSS
 *
 * @author    Sara Gerion <sara@superherocheesecake.com>
 * @package   App\Http\Middleware
 * @copyright Sara Gerion
 */
class XSS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $input = $request->all();
        // Strip HTML and PHP tags from the input values
        array_walk_recursive($input, function (&$input) {
            $input = strip_tags($input);
        });
        $request->merge($input);

        return $next($request);
    }
}
