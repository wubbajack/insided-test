<?php namespace App\Core\Http\Middleware;

use Closure;

use Illuminate\Http\Request;
use Illuminate\Config\Repository as Config;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class Whitelist
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Core\Http\Middleware
 * @copyright Chris Stolk
 * @since     17/03/16 21:09
 */
class Whitelist
{

    /**
     * @var Config
     */
    protected $config;

    /**
     * Whitelist constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Handles the request and checks whether it's whitelisted
     *
     * @param  Request  $request
     * @param  Closure $next
     * @return mixed
     * @throws AccessDeniedHttpException
     */
    public function handle($request, Closure $next)
    {
        if (!$this->isEnabled()) {
            return $next($request);
        }

        $request->setTrustedHeaderName(Request::HEADER_CLIENT_IP, $this->getForwardedHeader());
        $request->setTrustedProxies($this->getProxies());

        if (array_intersect($request->getClientIps(), $this->getAllowedIps())) {
            return $next($request);
        }

        throw new AccessDeniedHttpException($request->getClientIp() .' is not allowed.');
    }

    /**
     * Returns the forwarded header
     *
     * @return string
     */
    protected function getForwardedHeader()
    {
        return $this->config->get('whitelist.forwarded_header', 'X_FORWARDED_FOR');
    }

    /**
     * Returns an array of allowed IP's
     *
     * @return array
     */
    protected function getAllowedIps()
    {
        return $this->config->get('whitelist.ips', []);
    }

    /**
     * Returns all proxies set in the configuration
     *
     * @return array
     */
    protected function getProxies()
    {
        return $this->config->get('whitelist.proxies', []);
    }

    /**
     * Returs whether whitelisting is enabled
     *
     * @return bool
     */
    protected function isEnabled()
    {
        return (bool) $this->config->get('whitelist.enabled', false);
    }
}
