<?php namespace App\Core\Data\Repositories;

use App\Core\Data\Models\Post;
use App\Core\Handlers\PostImageHandler;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Container\Container;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class PostsRepository
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   Core\Data\Repositories
 * @copyright Chris Stolk
 * @since     02/07/16 15:13
 */
class PostsRepository extends EloquentRepository
{

    /**
     * @var Container
     */
    protected $container;

    /**
     * PostsRepository constructor.
     *
     * @param Post      $model
     * @param Container $container
     */
    public function __construct(Post $model, Container $container)
    {
        parent::__construct($model);
        $this->container = $container;
    }

    /**
     * Returns the total views
     *
     * @return int
     */
    public function getTotalViews()
    {
        return (int) $this->model->sum('views');
    }

    /**
     * Increments the amount of views for this post
     *
     * @param Post $post
     * @return Post
     */
    public function incrementViewsForPost(Post $post)
    {
        $post->views = $this->model
            ->newQuery()
            ->where($this->model->getKeyName(), $post->getKey())
            ->increment('views', 1);

        return $post;
    }

    /**
     * Returns all ordered by newest
     *
     * @return Collection
     */
    public function allOrderedByNewest()
    {
        return $this->model->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Creates a post from a request
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createFromRequest(Request $request)
    {
        /**
         * @var $handler PostImageHandler
         */
        $file    = $request->file('image');
        $handler = $this->container->make(PostImageHandler::class);

        return $this->create([
            'title'           => $request->input('title'),
            'image_type'      => $this->getImageTypeFromFile($file),
            'image_full'      => $handler->storeImage($file, $file->getClientOriginalExtension()),
            'image_thumbnail' => $handler->storeThumbnail($file)
        ]);
    }

    /**
     * Checks whether the image is animated or not
     *
     * @param UploadedFile $file
     * @return bool
     */
    protected function getImageTypeFromFile(UploadedFile $file)
    {
        $image = new \Imagick($file->getRealPath());
        return $image->getImageIterations() > 0 ? Post::IMAGE_TYPE_ANIMATED: Post::IMAGE_TYPE_STATIC;
    }
}
