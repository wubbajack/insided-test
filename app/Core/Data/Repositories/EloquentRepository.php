<?php namespace App\Core\Data\Repositories;

use App\Core\Interfaces\Repository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class EloquentRepository
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   Core\Data\Repositories
 * @copyright Chris Stolk
 * @since     02/07/16 14:56
 */
abstract class EloquentRepository implements Repository
{

    /**
     * @var Model
     */
    protected $model;

    /**
     * EloquentRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Returns the model
     *
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Returns all records
     *
     * @return Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Chunks the results
     *
     * @param int      $items
     * @param callable $callback
     * @return mixed
     */
    public function chunk($items, callable $callback)
    {
        return $this->model->chunk((int) $items, $callback);
    }

    /**
     * Finds a particular record for key
     *
     * @param  mixed $key
     * @return Model
     */
    public function find($key)
    {
        return $this->model->find($key);
    }

    /**
     * Lists a certain value (and key)
     *
     * @param string $value
     * @param null   $key
     * @return mixed
     */
    public function lists($value, $key = null)
    {
        return $this->model->lists($value, $key);
    }

    /**
     * Paginates the result
     *
     * @param  int $per_page
     * @return LengthAwarePaginator
     */
    public function paginate($per_page, $page = null)
    {
        return $this->model->paginate($per_page, ['*'], 'page', $page);
    }

    /**
     * Creates a new model
     *
     * @param  array $attributes
     * @return Model
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Updates the attributes of a set of models based on key and key attribute
     *
     * @param  array  $attributes
     * @param  mixed  $key
     * @param  null|string $key_attribute
     * @return Model
     */
    public function update(array $attributes, $key, $key_attribute = null)
    {
        $model = $this->find($key);
        $model->update($attributes);

        return $model;
    }

    /**
     * Deletes a record with a specific key
     *
     * @param  mixed $key
     * @return int Records deleted
     */
    public function delete($key)
    {
        return $this->model->destroy($key);
    }

    /**
     * Counts the amount of posts (with a possible constraint)
     *
     * @param  callable|null $constraint
     * @return int
     */
    public function count(callable $constraint = null)
    {
        $query = $this->model->newQuery();
        if ($constraint) {
            $query->where($constraint);
        }

        return $query->count();
    }
}
