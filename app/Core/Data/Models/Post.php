<?php namespace App\Core\Data\Models;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Core\Data
 * @copyright Chris Stolk
 * @since     2016-07-02 14:41
 */
class Post extends Model
{

    /**
     * Different image types
     */
    const IMAGE_TYPE_STATIC   = 1;
    const IMAGE_TYPE_ANIMATED = 2;

    /**
     * @var string
     */
    protected $table = 'posts';

    /**
     * @var array
     */
    protected $guarded = ['id', self::CREATED_AT, self::UPDATED_AT];

    /**
     * Returns the encoded key
     *
     * @return string
     */
    public function getEncodedKey()
    {
        $hasher = app(Hashids::class);

        return $hasher->encode($this->getKey());
    }

    /**
     * Decodes the key
     *
     * @param  string $key
     * @return int|string
     */
    public static function decodeKey($key)
    {
        $hasher = app(Hashids::class);
        $decoded_key = $hasher->decode($key);
        if (!is_array($decoded_key)) {
            throw new \InvalidArgumentException('Key is invalid');
        }

        return array_shift($decoded_key);
    }
}
