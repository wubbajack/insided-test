<?php

namespace App\Core\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{

    /**
     * The namespaces in which we register our routes
     *
     * @var array
     */
    protected $spaces = [
        'frontend' => [
            'namespace' => 'App\Frontend\Controllers',
            'file'      => 'Frontend/routes.php'
        ],

        'api' => [
            'namespace' => 'App\Api\Http\Controllers',
            'file'      => 'Api/routes.php'
        ]
    ];

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        foreach ($this->spaces as $space) {
            $this->registerRouteSpace($router, $space);
        }
    }

    /**
     * Registers the space for the route
     *
     * @param  Router $router
     * @param  array  $space
     * @return void
     */
    protected function registerRouteSpace(Router $router, array $space)
    {
        $namespace  = $space['namespace'];
        $route_file = app_path($space['file']);

        if (!file_exists($route_file)) {
            throw new \RuntimeException("Route file $route_file is missing");
        }

        $router->group(['namespace' => $namespace], function () use ($route_file) {
            require $route_file;
        });
    }
}
