<?php namespace App\Core\Providers;

use App\Core\Handlers\PostImageHandler;
use Hashids\Hashids;

use Intervention\Image\ImageManager;

use Illuminate\Validation\Validator;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Core\Providers
 * @copyright Chris Stolk
 * @since     2016-07-02
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPostImageHandler();
        $this->registerCustomValidationRules();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
            $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
        }

        $this->registerHashIds();
        $this->registerIntervention();
        $this->registerPostImageHandler();
    }

    /**
     * Registers hashed IDs
     */
    protected function registerHashIds()
    {
        $this->app->singleton(Hashids::class, function (Application $app) {
            return new Hashids(env('hashids_salt'));
        });
    }

    /**
     * Registers intervenion image
     */
    protected function registerIntervention()
    {
        $this->app->singleton(ImageManager::class, function (Application $app) {
            return new ImageManager(['driver' => 'imagick']);
        });
    }

    /**
     * Registers the post image handler
     */
    protected function registerPostImageHandler()
    {
        $this->app->singleton(PostImageHandler::class, function (Application $app) {
            $base_path = public_path('content');
            return new PostImageHandler($app->make(ImageManager::class), $base_path);
        });
    }

    /**
     * Registers custom validation rules
     */
    protected function registerCustomValidationRules()
    {
        \Validator::extend('image_size', function ($attribute, $value, $parameters, Validator $validator) {
            if (!($value instanceof \SplFileInfo)) {
                return false;
            }

            // List the height and width params
            list($width, $height) = array_map(function ($value) {
                return trim($value);
            }, $parameters);

            try {
                $image = app(ImageManager::class)->make($value->getRealPath());
                return $image->height() <= $height || $image->width() <= $width;
            } catch (\Exception $e) {
                return false;
            }
        }, 'Image size exceeded. Width max: 1920; Height max: 1080');

        \Validator::extend('filesize', function ($attribute, $value, $parameters, Validator $validator) {
            list ($filesize, $unit) = $parameters;
            $unit  = strtolower($unit);
            $units = [
                'b'  => 0,
                'kb' => 1 << 10,
                'mb' => 1 << 20,
                'gb' => 1 << 30
            ];

            if (!array_key_exists($unit, $units)) {
                return false;
            }

            $filesize = (int) ($filesize * $units[$unit]);
            return $value->getSize() <= $filesize;
        }, 'Filesize for :attribute is too large');
    }
}
