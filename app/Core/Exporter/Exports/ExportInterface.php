<?php namespace App\Core\Exporter\Exports;

/**
 * Class ExportInterface
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @copyright Chris Stolk
 * @since     11/02/16 14:49
 */
interface ExportInterface
{

    /**
     * Returns the data of the export
     * @return array
     */
    public function getData();

    /**
     * Returns a single line of data
     * @return array
     */
    public function getLine();

    /**
     * Returns the headers
     * @return array
     */
    public function getHeaders();
}
