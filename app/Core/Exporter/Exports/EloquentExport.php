<?php namespace App\Core\Exporter\Exports;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Export
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @copyright Chris Stolk
 * @since     11/02/16 14:49
 */
abstract class EloquentExport
{

    /**
     * @var Model
     */
    protected $source;

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var int
     */
    protected $limit = 100;

    /**
     * @var int
     */
    protected $current_page = 1;

    /**
     * EloquentExport constructor.
     *
     * @param Model $source
     */
    public function __construct(Model $source)
    {
        $this->source     = $source;
        $this->collection = Collection::make([]);
    }

    /**
     * Returns a single model
     * @return Model|null
     */
    public function getSingle()
    {
        $this->loadCollection();
        return $this->collection->shift();
    }

    /**
     * Returns the source
     *
     * @return Model
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Sets the source
     * @param Model $source
     * @return $this
     */
    public function setSource(Model $source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * Returns the current limit per page
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Sets the limit
     *
     * @param  int $limit
     * @return EloquentExport
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * Loads the collection
     * @return void
     */
    protected function loadCollection()
    {
        if (!$this->collection->isEmpty()) {
            return;
        }

        $this->collection = $this->source->forPage($this->current_page++, $this->limit)->get();
    }
}
