<?php namespace App\Core\Exporter\Exports;

use App\Core\Data\Models\Post;

/**
 * Class PostsExport
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Core\Exporter\Exports
 * @copyright Chris Stolk
 * @since     03/07/16 17:00
 */
class PostsExport extends EloquentExport implements ExportInterface
{

    /**
     * Returns the data of the export
     *
     * @return array
     */
    public function getData()
    {
        return [];
    }

    /**
     * Returns a single line of data
     *
     * @return array
     */
    public function getLine()
    {
        /**
         * @var $model Post
         */
        if (!$model = $this->getSingle()) {
            return false;
        }

        $data = [];
        foreach ($this->getHeaders() as $key) {
            $method = 'get' . camel_case($key);

            if (method_exists($this, $method)) {
                $data[] = $this->$method($model);
            } else {
                $data[] = $model->getAttribute($key);
            }
        }

        return $data;
    }

    /**
     * Returns the headers
     *
     * @return array
     */
    public function getHeaders()
    {
        return [
            'title',
            'image',
            'posted_on',
        ];
    }

    /**
     * Returns the URL to the image
     *
     * @param  Post $post
     * @return string
     */
    protected function getImage(Post $post)
    {
        return url('content/' . $post->getAttribute('image_full'));
    }

    /**
     * Returns the date the post was created in Rfc3339 format
     *
     * @param  Post $post
     * @return string
     */
    protected function getPostedOn(Post $post)
    {
        return $post->created_at->toRfc3339String();
    }
}
