<?php namespace App\Core\Exporter;

use Illuminate\Filesystem\Filesystem;
use App\Core\Exporter\Exports\ExportInterface;

/**
 * Class Exporter
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @copyright Chris Stolk
 * @since     11/02/16 14:48
 */
class CsvExporter implements ExporterInterface
{

    /**
     * @var string
     */
    protected $enclosure = "\"";

    /**
     * @var string
     */
    protected $delimiter = ',';

    /**
     * Exporter constructor.
     *
     * @param Filesystem $fs
     */
    public function __construct(Filesystem $fs)
    {
        $this->fs = $fs;
    }

    /**
     * Saves the export to a file or resource
     *
     * @param  ExportInterface|string $export
     * @param  string|resource $file
     * @return mixed
     */
    public function save(ExportInterface $export, $file)
    {
        $resource = $this->getResource($file);
        $headers  = $export->getHeaders();

        // Write the headers
        if ($headers) {
            $this->writeLine($resource, $headers);
        }

        // Write the data
        while ($data = $export->getLine()) {
            $this->writeLine($resource, $data);
        }

        return $export;
    }

    /**
     * Outputs the csv export
     *
     * @param  ExportInterface $export
     * @return void
     */
    public function output(ExportInterface $export)
    {
        echo implode($this->delimiter, $this->enclose($export->getHeaders()));

        while ($line = $export->getLine()) {
            echo PHP_EOL.implode($this->delimiter, $this->enclose($line));
        }
    }

    /**
     * Sets the enclosure
     * @param string $enclosure
     */
    public function setEnclosure($enclosure)
    {
        $this->enclosure = $enclosure;
    }

    /**
     * Sets the delimiter
     *
     * @param string $delimiter
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    /**
     * Writes the line
     * @param  resource $resource
     * @param  array    $data
     * @return bool
     */
    protected function writeLine($resource, array $data)
    {
        if (!is_resource($resource)) {
            throw new \InvalidArgumentException('No resource given');
        }

        fputcsv($resource, $data, $this->delimiter, $this->enclosure);
    }

    /**
     * Returns the resource
     *
     * @param string $file
     * @return resource
     */
    protected function getResource($file)
    {
        if (is_resource($file)) {
            return $file;
        }

        if (!$this->fs->exists($file)) {
            touch($file);
        }

        return fopen($file, 'w+');
    }

    /**
     * Encloses the data
     * @param  array $data
     * @return array
     */
    protected function enclose($data)
    {
        return array_map(function ($item) {
            if ($item === null) {
                return $item;
            }

            return "{$this->enclosure}{$item}{$this->enclosure}";
        }, $data);
    }
}
