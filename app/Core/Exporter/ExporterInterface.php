<?php namespace App\Core\Exporter;

use App\Core\Exporter\Exports\ExportInterface;

/**
 * Class ExporterInterface
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   Shcc\Exporter
 * @copyright Chris Stolk
 * @since     11/02/16 20:14
 */
interface ExporterInterface
{

    /**
     * Saves the export to a file (or stream)
     *
     * @param  ExportInterface $export
     * @param                  $file
     * @return mixed
     */
    public function save(ExportInterface $export, $file);

    /**
     * Outputs the export, useful for streaming
     * @param  ExportInterface $export
     * @return void
     */
    public function output(ExportInterface $export);
}
