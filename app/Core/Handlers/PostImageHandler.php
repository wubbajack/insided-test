<?php namespace App\Core\Handlers;

use Intervention\Image\Constraint;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

/**
 * Class PostImageHandler
 *
 * @author    Chris Stolk <stolkchris@gmail.com>
 * @package   App\Core\Handlers
 * @copyright Chris Stolk
 * @since     03/07/16 11:23
 */
class PostImageHandler
{

    /**
     * @var ImageManager
     */
    protected $image;

    /**
     * @var MimeTypeGuesser
     */
    protected $guesser;

    /**
     * @var string
     */
    protected $base_path;

    /**
     * @var int
     */
    protected $thumb_width = 640;

    /**
     * @var int
     */
    protected $thumb_height = 480;

    /**
     * PostImageHandler constructor.
     *
     * @param ImageManager $imageManager
     * @param string       $base_path
     */
    public function __construct(ImageManager $imageManager, $base_path)
    {
        $this->image = $imageManager;
        $this->setBasePath($base_path);

        // Not instantiable using the container, follows singleton principle in object
        $this->guesser = MimeTypeGuesser::getInstance();
    }

    /**
     * Returns the base path
     *
     * @return string
     */
    public function getBasePath()
    {
        return $this->base_path;
    }

    /**
     * Gets the thumbnail width
     *
     * @return int
     */
    public function getThumbWidth()
    {
        return $this->thumb_width;
    }


    /**
     * Sets the thumbnail width
     *
     * @param int $thumb_width
     * @return PostImageHandler
     */
    public function setThumbWidth($thumb_width)
    {
        $this->thumb_width = (int) $thumb_width;
        return $this;
    }

    /**
     * Gets the thumbnail height
     *
     * @return int
     */
    public function getThumbHeight()
    {
        return $this->thumb_height;
    }

    /**
     * Sets the thumbnail height
     *
     * @param int $thumb_height
     * @return PostImageHandler
     */
    public function setThumbHeight($thumb_height)
    {
        $this->thumb_height = (int) $thumb_height;
        return $this;
    }

    /**
     * Stores the original image
     *
     * @param \SplFileInfo $file
     * @param              $extension
     * @return string
     * @throws \Exception
     */
    public function storeImage(\SplFileInfo $file, $extension)
    {
        $filename = $this->makeFilename($file, null, $extension);
        $path     = $this->makePath($filename);

        try {
            $this->copyFile($file, $path);
        } catch (\Exception $e) {
            $this->cleanup($path);
            throw $e;
        }

        return str_replace($this->base_path, '', $path);
    }

    /**
     * Stores the thumbnail for the image
     *
     * @param \SplFileInfo $file
     * @return string
     * @throws \Exception
     */
    public function storeThumbnail(\SplFileInfo $file)
    {
        $filename = $this->makeFilename($file, 'thumbnail_', '.jpg');
        $path     = $this->makePath($filename);

        $image  = $this->image->make($file->getRealPath());
        $height = $image->height();
        $width  = $image->width();

        if ($height > $this->thumb_height || $width > $this->thumb_width) {
            $constraint = function (Constraint $constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            };

            if ($height > $width) {
                $image->heighten($this->thumb_height, $constraint);
            } else {
                $image->widen($this->thumb_width, $constraint);
            }
        }

        try {
            file_put_contents($path, $image->encode('jpeg', 75));
        } catch (\Exception $e) {
            $this->cleanup($path);
            throw $e;
        }

        return str_replace($this->base_path, '', $path);
    }

    /**
     * Copies a file to a target path
     *
     * @param  \SplFileInfo $file
     * @param  string       $path
     * @return bool
     */
    protected function copyFile(\SplFileInfo $file, $path)
    {
        $source = fopen($file, 'r');
        $target = fopen($path, 'w+');

        stream_copy_to_stream($source, $target);
        fclose($source);
        fclose($target);

        return true;
    }

    /**
     * Checks whether the file is a Gif
     *
     * @param \SplFileInfo $file
     * @return bool
     */
    protected function isGif(\SplFileInfo $file)
    {
        return $this->guesser->guess($file->getRealPath()) == 'image/gif';
    }

    /**
     * Sets the base path
     *
     * @param  string $base_path
     * @return void
     */
    protected function setBasePath($base_path)
    {
        if (substr($base_path, -1, 1) != '/') {
            $base_path .= '/';
        }

        $this->base_path = $base_path;
    }

    /**
     * Makes the path
     *
     * @param  string $filename
     * @return string
     */
    protected function makePath($filename)
    {
        $path = $this->base_path . $this->shard($filename);
        $dir  = dirname($path);

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        return $path;
    }

    /**
     * Returns the filename
     *
     * @param  \SplFileInfo $file
     * @param  string       $prefix
     * @param  null         $extension
     * @return string
     */
    protected function makeFilename(\SplFileInfo $file, $prefix = null, $extension = null)
    {
        if ($extension && substr($extension, 0, 1) != '.') {
            $extension = '.'.$extension;
        }

        return hash('sha1', $prefix . microtime() . $file->getBasename()) . $extension;
    }

    /**
     * Shards the filename (for a correct path)
     *
     * @param  string $filename
     * @param  int    $parts
     * @param  int    $size
     * @return mixed
     */
    protected function shard($filename, $parts = 3, $size = 4)
    {
        $shards = [];
        for ($i = 0; $i < $parts; $i++) {
            $shards[] = substr($filename, 0 + ($size * $i), $size);
        }

        // Quicker than array merge
        $shards[] = $filename;
        return implode(DIRECTORY_SEPARATOR, $shards);
    }

    /**
     * If a file has been created we perform cleanup
     *
     * @param string $path
     */
    protected function cleanup($path)
    {
        if (!is_file($path)) {
            return;
        }

        @unlink($path);
    }
}
