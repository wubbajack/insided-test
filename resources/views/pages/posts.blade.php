<div class="container">
    <?= $form; ?>
</div>
<div class="container posts-container">
    @each('partials.posts.list_template', $posts, 'post')
</div>

<script type="text/template" id="post-template">
    <?=$template;?>
</script>