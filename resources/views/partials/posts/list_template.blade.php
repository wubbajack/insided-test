<div class="row" style="margin-bottom:20px;">
    <div class="col-xs-6 col-xs-offset-3">
        @if($post->title)
            <div class="text-center">{{ $post->title }}</div>
        @endif
        <a href="{{ route('posts.single', $post->getEncodedKey()) }}">
            <img src="{{ url('content/'. $post->image_thumbnail) }}" class="img-rounded">
        </a>
    </div>
</div>