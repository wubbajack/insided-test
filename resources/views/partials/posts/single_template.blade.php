<div class="row">
    <div class="col-xs-12 col-md-12 col-lg-12">
        @if($post->title)
            <div class="text-center">{{ $post->title }}</div>
        @endif
        <div class="center-block" style="text-align:center;">
            <img src="{{ url('content/'. $post->image_full) }}" class="img-rounded">
        </div>
    </div>
</div>