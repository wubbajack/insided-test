<div class="row" style="margin-bottom:20px;">
    <div class="col-xs-6 col-xs-offset-3">
        <% if (title) { %>
        <div class="text-center"><%= title %></div>
        <% } %>

        <a href="<%= url %>" target="_blank">
            <img src="<%= image.thumbnail %>" class="img-rounded">
        </a>
    </div>
</div>