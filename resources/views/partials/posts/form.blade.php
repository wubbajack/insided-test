<div class="row">
    <div class="col-md-offset-2 col-sm-8">
        <form class="form-horizontal posts-form" enctype="multipart/form-data" action="{{ url('/api/posts') }}" method="post">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control input-posts_input-title" name="title" placeholder="Title">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Image</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control input-posts_input-image" name="image">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Upload</button>
                </div>
            </div>
        </form>
    </div>
</div>
