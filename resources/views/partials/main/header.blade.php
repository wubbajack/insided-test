<nav class="navbar navbar-ct-primary" role="navigation-demo">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">InSided test</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigation">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <button href="#" class="dropdown-toggle btn" data-toggle="dropdown">Export <b class="caret"></b></button>
                    <!--                                  You can add classes for different colours on the next element -->
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-header">Select export to run</li>
                        <li><a href="{{ route('posts.export', 'csv') }}" target="_blank">CSV</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('posts.export', 'zip') }}" target="_blank">Zip</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        Posts: <span class="component-posts"><?= $posts;?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        Views: <span class="component-views"><?= $views;?></span>
                    </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>