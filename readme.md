# Insided test app
Setup instructions for the test application:

## Laravel basic setup
For basic Laravel installation details please go to the [documentation page](https://laravel.com/docs/5.2/installation)

## Project requirements:
 - PHP 5.6 (untested on 5.5)
 - Imagick

## Basic Setup
 - Create a vhost that points to `PROJECT_ROOT/public`. Apache is the easier choice for this particular project.
 - Copy .env.example to .env
    - Fill in your DB credentials and name and set the app environment to `production`
 - Run `./composer.phar install`. This will install all depdencies needed to run the application.
 - Make sure that `PROJECT_ROOT/public/content` is writable
 - Go to `yourhost.local` to see the application :)
