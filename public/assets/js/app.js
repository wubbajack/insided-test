$(document).on('ready', function () {

    var template       = document.getElementById('post-template');
    var postsContainer = document.querySelector('.posts-container');

    var form = $('.posts-form');

    var posts = $('.component-posts');
    var views = $('.component-views');

    var inputs = {
        title: $('input.input-posts_input-title'),
        image: $('input.input-posts_input-image')
    };

    function addPostToView(post)
    {
        var element = document.createElement('template');
        var compiled = _.template(template.innerHTML);

        $(postsContainer).prepend(compiled(post));
        element.innerHTML = compiled(post);
    }

    function displayInputErrors(errors)
    {
        if (!errors) {
            inputs.image.parents('.form-group').addClass('has-error');
            inputs.title.parents('.form-group').addClass('has-error');
        }

        if (errors.image) {
            inputs.image.parents('.form-group').addClass('has-error');
        }

        if (errors.title) {
            inputs.title.parents('.form-group').addClass('has-error');
        }
    }

    function clearErrors()
    {
        inputs.image.parents('.form-group').removeClass('has-error');
        inputs.title.parents('.form-group').removeClass('has-error');
    }

    function updateMetaInformation()
    {
        $.get('/api/posts/meta')
            .done(function (data) {
                posts.text(data.total);
                views.text(data.views);
            })
            .fail(function () {
                console.error('Failed retrieving posts meta information');
            });
    }

    setInterval(updateMetaInformation, 15000);
    form.on('submit', function (e) {
        e.preventDefault();

        var data = new FormData(this);
        $.ajax({
            method: 'POST',
            url: this.getAttribute('action'),
            data: data,
            contentType: false,
            processData: false,
            success: function (post) {
                addPostToView(post);
                clearErrors();

                updateMetaInformation();
            },
            error: function (response) {
                if (response.status == 422) {
                    var data = JSON.parse(response.responseText);
                    displayInputErrors(data);
                } else {
                    displayInputErrors();
                }
            }
        });

        return false;
    });
});