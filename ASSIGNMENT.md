# Insided test application
Hi there! Thanks for taking the time to look through my code. I hope it'll live up to the standards the company has!

# NOTES
 - You might find a column named `image_type` in the database. This was designed to make a distinction between animated and static images. Due to time constraints I've not been able to implement reliable detection for animated Gif's (have to get dirty with the command line tools for that one).
 - There's a single test for testing the early stages of the posts repository. It's not been updated, also due to time constraints. It is however still working.
 - I've omitted the part about scaling the app, as I find it much more interesting to tell you about this in person (a diagram will be able to tell part of the story :))

## Implemented
The following requirements of the assignment have been implemented:
 - Uploading of "Posts" with the mimetypes jpg, png and gif (animated and still)
    - The title of posts are fully optional and have a restriction of 100 characters
    - Images have a filesize restriction of 20mb
    - Images have a size restriction of w <= 1920 and h <= 1080
    - Once uploaded, a new post is added to the page
 - The list of posts is sorted with the newest post on top
 - Posts increment with each new posted post
 - Views increment on each "single" post page visit
 - Every 15 seconds, the posts and views will be refreshed

The following **bonus** parts of the assignment have been implemented:
 - Support for png and gif
 - Support for images up to 20mb
 - Exporting to a zip file
 - Posts and views refresh every 15 seconds
 - Implemented a basic API without authentication to demonstrate possibilities
    - Please see the file \_api/blueprint.apib for a detailed blueprint.
    - It can also be found [online](http://docs.insidedapi.apiary.io/)

I've not implemented an Excel export because of [this]([https://support.office.com/en-us/article/Import-or-export-text-txt-or-csv-files-5250ac4c-663c-47ce-937b-339e391393ba#bmimport_data_from_a_text_file_by_openi])

## Design decisions
### Framework
The framework that has been used is Laravel 5.2. The application structure has been changed from the Laravel defaults
to provide separate working- and namespaces for Api and Frontend, and a common Core namespace.

All things shared between API and Frontend can be stored in the Core namespace as it's core to the entire application
and not just the API or Frontend.

### Dependency Injection
This application relies heavily on dependency injection. To do this, Laravel's own DI container is used to a great extent.

### Repository
For posts I've implemented a repository to implement a clear and concise interface to the data layer. Laravel's
eloquent models are incredibly powerful but it's also very easy to lose track of the responsibility. Any functionality
written in the model itself binds it to eloquent. This in turn will require a lot more refactoring should the DBAL layer
change to something like Doctrine.

### Image handling
The image handler is slightly abstracted so that it can deal with files from any source (not just inside the Request/Response cycle).
Imagick has been used to resize images as it's the faster option to GD. For Gif files, the data is copied using stream copy to keep memore usage as low as possible and the throughput high.
This is done to preserve animated Gif's as the library used for image manipulation saves them as a still image.

### API
I've opted to start off with creating an API right off the bat (instead of creating a traditional web-app) to work with.
Due to time constraints, only the creating of posts is using the API.

The API is (as far as I can tell) RESTful, with the minor exception of the /posts/meta endpoint. There is one resource available,
the `posts` resource. Please check the API blueprint for more details on how to access it.

#### Presenters
There's a difference between the actual data stored in the DB and the data that is returned in an API call. To make sure
the response of the API objects are controlled I've implemented a presenter for posts. This allows for greater flexility
because you are not strictly bound to the data stored in the model.

A good example would be that images have a relative path to the public path of the project. We don't want to have the full
server path or the entire URL stored in the DB. The presenter adds this information for us, making it more flexible.

### Exporter
Exporting data can always be a bit tricky which is the reason I've implemented streamed responses for both CSV and Zip
exports. This is to ensure that the exporting process keeps performing.

Another step to make sure the exports keeps performing is the chunking of data retrieval. Instead of working with thousands
of objects right away, they are chunked into smaller pieces and processed per chunk.
