<?php
/*
|--------------------------------------------------------------------------
| Detect The Application Environment
|--------------------------------------------------------------------------
|
| Laravel takes a dead simple approach to your application environments
| so you can just specify a machine name for the host that matches a
| given environment, then we will automatically detect it for you.
|
*/
$env = $app->detectEnvironment(function () {

    /**
     * Use directories to find the correct production environment.
     */
    $environment_directories = [
        'production'  => [],
        'staging'     => [],
        'development' => '.d.shcc.nl',
    ];

    foreach ($environment_directories as $environment => $directories) {
        if (!is_array($directories)) {
            $directories = [$directories];
        }

        foreach ($directories as $directory) {
            if (strstr(__DIR__, $directory) !== false) {
                $current_env = $environment;
                break;
            }
        }

        if (isset($current_env)) {
            break;
        }
    }

    if (!isset($current_env)) {
        /**
         * Use hostname to§ detect enviroment
         */
        $environments = [
            'local'       => ['superherocheesecake-development', 'local'],
            'development' => ['vm4502.vellance.net', 'vm4628.vellance.net'],
        ];

        $current_host = gethostname();
        foreach ($environments as $environment => $hosts) {
            foreach ((array)$hosts as $host) {
                $isThisMachine = str_is($host, $current_host);
                if ($isThisMachine) {
                    $current_env = $environment;
                }
            }
        }
    }


    // Sets the value of the Environment as the current environment
    putenv("APP_ENV=$current_env");

    return $current_env;

});

function loadConfiguration()
{

}