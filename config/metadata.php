<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Project Title
    |--------------------------------------------------------------------------
    |
    | The default Project Title. Will appear in the <title> tag of the wrapper
    | by default. Assign a title variable to the wrapper to overwrite this.
    |
    */

    'title' =>  'Title of this project',


    /*
    |--------------------------------------------------------------------------
    | Project Description
    |--------------------------------------------------------------------------
    |
    | The default Project description. Will appear in the <description> tag of
    | the wrapper by default. Assign a description variable to the wrapper
    | to overwrite this.
    |
    */

    'description'   =>  'Description of this project',


    /*
    |--------------------------------------------------------------------------
    | Project Keywords
    |--------------------------------------------------------------------------
    |
    | The default keywords of this project. Will appear in the <keywords> tag
    | of the wrapper by default. Assign a keywords variable to the wrapper to
    | overwrite this.
    |
    */

    'keywords'   =>  'keyword, another',


    /*
    |--------------------------------------------------------------------------
    | Open Graph image
    |--------------------------------------------------------------------------
    |
    | The image which will appear on Facebook and twitter shares. Should be the
    | full URL to the image. If left commented out, will not appear in the
    | template
    |

    'og_image' => ''

    */


    /*
    |--------------------------------------------------------------------------
    | Twitter Meta data
    |--------------------------------------------------------------------------
    |
    | Twitter site owner and creator. Should be filled in with the appropriate
    | twitter handles. If left commented out, will not appear in the template
    |

    'twitter' => [
        'site' => '',
        'creator' => '',
    ],

    */


    /*
    |--------------------------------------------------------------------------
    | FB Data
    |--------------------------------------------------------------------------
    |
    | The admin ids are the default Facebook accounts which are added as admin
    | in the meta data. The default account Ids are Jordi Romkema and Niels van Esch
    |
    */

    'fb' => [
        'app_id' => NULL,
        'admin_ids'  =>  array('1048116169', '100000305063209'),
    ],


    /*
    |--------------------------------------------------------------------------
    | Cache Buster ID
    |--------------------------------------------------------------------------
    |
    | This number is by default added to the CSS and JS files as get variable.
    | If some changes are made to one or more of these files, this number can
    | be changed to force all clients to download the files again.
    |
    */

    'cache_buster'  =>  1,


    /*
    |--------------------------------------------------------------------------
    | Server variables
    |--------------------------------------------------------------------------
    |
    | The server array contains some important URLs we can use as a variable in
    | the app.
    |
    */

    'urls' =>  array(
        'main'      => ENV('APP_URL'),
        'images'    => ENV('APP_URL') . '/static/img',
        'css'       => ENV('APP_URL') . '/static/css',
        'js'        => ENV('APP_URL') . '/static/js',
        'uploads'   => ENV('APP_URL') . '/content/uploads',
        'generated' => ENV('APP_URL') . '/content/generated'
    ),
];