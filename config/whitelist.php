<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Control whitelist
    |--------------------------------------------------------------------------
    |
    | This configuration option controls whether the whitelist is
    | enabled or disabled.
    |
    | By default it's disabled
    |
    */

    'enabled' => ENV('WHITELIST_ENABLED', true),

    /*
    |--------------------------------------------------------------------------
    | Fowarded IP header
    |--------------------------------------------------------------------------
    |
    | The header that contains the ip that has been forwarded through the proxy
    |
    */

    'forwarded_header' => 'X_FORWARDED_FOR',

    /*
    |--------------------------------------------------------------------------
    | Trusted proxies
    |--------------------------------------------------------------------------
    |
    | Proxies which are trusted by the application
    |
    */

    'proxies' => [],

    /*
    |--------------------------------------------------------------------------
    | IP's
    |--------------------------------------------------------------------------
    |
    | IP's that are whitelisted when enabled
    |
    */

    'ips' => [
        // Local IP addresses
        '127.0.0.1',
        '10.0.0.1',

        // Office IP's
        '213.127.238.135',  // Old office IP
        '145.131.185.70',   // New office IP
    ]
];
